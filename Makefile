SHELL := /bin/bash

current_dir := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(shell dirname $(current_dir))
sources := $(shell find src/ -type f -name "*.res")
PATH := $(current_dir)/node_modules/.bin/:$(PATH)


format:
	rescript format -all

.PHONY: format

.PHONY: icons

node_modules yarn.lock: package.json
	yarn install
	touch node_modules

compile: $(sources) yarn.lock node_modules
	@if [ -n "$(INSIDE_EMACS)" ]; then \
	    NINJA_ANSI_FORCED=0 rescript build -with-deps; \
	else \
		rescript build -with-deps; \
	fi


run: compile
	esbuild tests/index.bs.js --bundle --target=firefox80,chrome80 --servedir=tests --outfile=tests/index.js

.PHONY: run
