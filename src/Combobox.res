type renderProps<'value> = {
  @as("open") open_: bool,
  disabled: bool,
  value: 'value,
  activeIndex: Js.nullable<int>,
  activeOption: Js.nullable<'value>,
}
@module("@headlessui/react") @react.component
external make: (
  @as("as") ~as_: 'asType=?,
  ~disabled: bool=?,
  ~value: 'value=?,
  ~defaultValue: 'value=?,
  ~by: ('value, 'value) => bool=?,
  ~onChange: 'value => unit=?,
  ~name: string=?,
  ~nullable: bool=?,
  ~multiple: bool=?,
  ~className: string=?,
  ~children: React.element,
) => React.element = "Combobox"

module Render = {
  @module("@headlessui/react") @react.component
  external make: (
    @as("as") ~as_: 'asType=?,
    ~disabled: bool=?,
    ~value: 'value=?,
    ~defaultValue: 'value=?,
    ~by: ('value, 'value) => bool=?,
    ~onChange: 'value => unit=?,
    ~name: string=?,
    ~nullable: bool=?,
    ~multiple: bool=?,
    ~className: string=?,
    ~children: renderProps<'value> => React.element,
  ) => React.element = "Combobox"
}

module Input = {
  type renderProps = {@as("open") open_: bool, disabled: bool}
  @module("@headlessui/react") @react.component @scope("Combobox")
  external make: (
    @as("as") ~as_: 'asType=?,
    ~className: string=?,
    ~displayValue: 'value => string=?,
    ~onChange: 'event => unit=?,
  ) => React.element = "Button"
}

module Button = {
  type renderProps<'v> = {@as("open") open_: bool, disabled: bool, value: 'v}
  @module("@headlessui/react") @react.component @scope("Combobox")
  external make: (
    @as("as") ~as_: 'asType=?,
    ~className: string=?,
    ~children: React.element=?,
  ) => React.element = "Button"

  module Render = {
    @module("@headlessui/react") @react.component @scope("Combobox")
    external make: (
      @as("as") ~as_: 'asType=?,
      ~className: string=?,
      ~children: renderProps<'value> => React.element=?,
    ) => React.element = "Button"
  }
}

module Label = {
  type renderProps = {@as("open") open_: bool, disabled: bool}
  @module("@headlessui/react") @react.component @scope("Combobox")
  external make: (
    @as("as") ~as_: 'asType=?,
    ~className: string=?,
    ~children: renderProps => React.element,
  ) => React.element = "Label"
}

module Options = {
  type renderProps = {@as("open") open_: bool}
  @module("@headlessui/react") @react.component @scope("Combobox")
  external make: (
    @as("as") ~as_: 'asType=?,
    ~static: bool=?,
    ~unmount: bool=?,
    ~hold: bool=?,
    ~className: string=?,
    ~children: React.element,
  ) => React.element = "Options"
}

module Option = {
  type renderProps = {active: bool, selected: bool, disabled: bool}
  @module("@headlessui/react") @react.component @scope("Combobox")
  external make: (
    @as("as") ~as_: 'asType=?,
    ~value: 'value=?,
    ~disabled: bool=?,
    ~className: renderProps => string=?,
    ~children: renderProps => React.element,
  ) => React.element = "Option"
}
