open Prelude

module Icons = {
  module EditInactiveIcon = {
    type props = ReactDOMRe.props

    let make = props =>
      <svg {...props} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M4 13V16H7L16 7L13 4L4 13Z" fill="#EDE9FE" stroke="#A78BFA" strokeWidth="2" />
      </svg>
  }

  module EditActiveIcon = {
    type props = ReactDOMRe.props

    let make = props =>
      <svg {...props} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M4 13V16H7L16 7L13 4L4 13Z" fill="#8B5CF6" stroke="#C4B5FD" strokeWidth="2" />
      </svg>
  }

  module DuplicateInactiveIcon = {
    type props = ReactDOMRe.props

    let make = props =>
      <svg {...props} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M4 4H12V12H4V4Z" fill="#EDE9FE" stroke="#A78BFA" strokeWidth="2" />
        <path d="M8 8H16V16H8V8Z" fill="#EDE9FE" stroke="#A78BFA" strokeWidth="2" />
      </svg>
  }

  module DuplicateActiveIcon = {
    type props = ReactDOMRe.props

    let make = props =>
      <svg {...props} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M4 4H12V12H4V4Z" fill="#8B5CF6" stroke="#C4B5FD" strokeWidth="2" />
        <path d="M8 8H16V16H8V8Z" fill="#8B5CF6" stroke="#C4B5FD" strokeWidth="2" />
      </svg>
  }

  module ArchiveInactiveIcon = {
    type props = ReactDOMRe.props

    let make = props =>
      <svg {...props} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect x="5" y="8" width="10" height="8" fill="#EDE9FE" stroke="#A78BFA" strokeWidth="2" />
        <rect x="4" y="4" width="12" height="4" fill="#EDE9FE" stroke="#A78BFA" strokeWidth="2" />
        <path d="M8 12H12" stroke="#A78BFA" strokeWidth="2" />
      </svg>
  }

  module ArchiveActiveIcon = {
    type props = ReactDOMRe.props

    let make = props =>
      <svg {...props} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect x="5" y="8" width="10" height="8" fill="#8B5CF6" stroke="#C4B5FD" strokeWidth="2" />
        <rect x="4" y="4" width="12" height="4" fill="#8B5CF6" stroke="#C4B5FD" strokeWidth="2" />
        <path d="M8 12H12" stroke="#A78BFA" strokeWidth="2" />
      </svg>
  }

  module MoveInactiveIcon = {
    type props = ReactDOMRe.props

    let make = props =>
      <svg {...props} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M10 4H16V10" stroke="#A78BFA" strokeWidth="2" />
        <path d="M16 4L8 12" stroke="#A78BFA" strokeWidth="2" />
        <path d="M8 6H4V16H14V12" stroke="#A78BFA" strokeWidth="2" />
      </svg>
  }

  module MoveActiveIcon = {
    type props = ReactDOMRe.props

    let make = props =>
      <svg {...props} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M10 4H16V10" stroke="#C4B5FD" strokeWidth="2" />
        <path d="M16 4L8 12" stroke="#C4B5FD" strokeWidth="2" />
        <path d="M8 6H4V16H14V12" stroke="#C4B5FD" strokeWidth="2" />
      </svg>
  }

  module DeleteInactiveIcon = {
    type props = ReactDOMRe.props

    let make = props =>
      <svg {...props} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect x="5" y="6" width="10" height="10" fill="#EDE9FE" stroke="#A78BFA" strokeWidth="2" />
        <path d="M3 6H17" stroke="#A78BFA" strokeWidth="2" />
        <path d="M8 6V4H12V6" stroke="#A78BFA" strokeWidth="2" />
      </svg>
  }

  module DeleteActiveIcon = {
    type props = ReactDOMRe.props

    let make = props =>
      <svg {...props} viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect x="5" y="6" width="10" height="10" fill="#8B5CF6" stroke="#C4B5FD" strokeWidth="2" />
        <path d="M3 6H17" stroke="#C4B5FD" strokeWidth="2" />
        <path d="M8 6V4H12V6" stroke="#C4B5FD" strokeWidth="2" />
      </svg>
  }
}

module Card = {
  @react.component
  let make = (
    ~gradientFrom: string,
    ~gradientTo: string,
    ~className: string,
    ~children: React.element,
  ) =>
    <div
      className={`relative w-1/2 min-h-1/4 h-1/4 max-w-2xl min-w-min
              rounded-xl flex flex-col items-center
              overflow-hidden
              pt-24 pl-8 pr-8 pb-8
              bg-gradient-to-r from-${gradientFrom} to-${gradientTo}`}>
      <div className="w-full h-[392px]">
        <div className> children </div>
      </div>
    </div>
}

module MenuExample = {
  open Icons

  @react.component
  let make = () =>
    <Card gradientTo="indigo-500" gradientFrom="purple-500" className="pt-16 text-white">
      <div className="absolute top-8 right-8 w-56 text-right">
        <Menu as_="div" className="relative inline-block text-left">
          <Menu.Button.Render
            className="inline-flex w-full justify-center rounded-md bg-black bg-opacity-20 px-4 py-2 text-sm font-medium text-white hover:bg-opacity-30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75">
            {({open_}) => <>
              <span className="font-bold"> {React.string("Options")} </span>
              {if open_ {
                <Heroicons.Solid.ChevronUpIcon
                  className="ml-2 -mr-1 h-5 w-5 text-violet-200 hover:text-violet-100"
                  ariaHidden=true
                />
              } else {
                <Heroicons.Solid.ChevronDownIcon
                  className="ml-2 -mr-1 h-5 w-5 text-violet-200 hover:text-violet-100"
                  ariaHidden=true
                />
              }}
            </>}
          </Menu.Button.Render>
          <Transition
            as_={React.Fragment.make}
            enter="transition ease-out duration-100"
            enterFrom="transform opacity-0 scale-95"
            enterTo="transform opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="transform opacity-100 scale-100"
            leaveTo="transform opacity-0 scale-95">
            <Menu.Items
              className="absolute right-0 mt-2 w-56 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
              <div className="px-1 py-1 ">
                <Menu.Item>
                  {({active}) =>
                    <button
                      className={`${active
                          ? "bg-violet-500 text-white"
                          : "text-gray-900"} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                      {active
                        ? <EditActiveIcon className="mr-2 h-5 w-5" ariaHidden=true />
                        : <EditInactiveIcon className="mr-2 h-5 w-5" ariaHidden=true />}
                      {React.string("Edit")}
                    </button>}
                </Menu.Item>
                <Menu.Item>
                  {({active}) =>
                    <button
                      className={`${active
                          ? "bg-violet-500 text-white"
                          : "text-gray-900"} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                      {active
                        ? <DuplicateActiveIcon className="mr-2 h-5 w-5" ariaHidden=true />
                        : <DuplicateInactiveIcon className="mr-2 h-5 w-5" ariaHidden=true />}
                      {React.string("Duplicate")}
                    </button>}
                </Menu.Item>
              </div>
              <div className="px-1 py-1">
                <Menu.Item>
                  {({active}) =>
                    <button
                      className={`${active
                          ? "bg-violet-500 text-white"
                          : "text-gray-900"} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                      {active
                        ? <ArchiveActiveIcon className="mr-2 h-5 w-5" ariaHidden=true />
                        : <ArchiveInactiveIcon className="mr-2 h-5 w-5" ariaHidden=true />}
                      {React.string("Archive")}
                    </button>}
                </Menu.Item>
                <Menu.Item>
                  {({active}) =>
                    <button
                      className={`${active
                          ? "bg-violet-500 text-white"
                          : "text-gray-900"} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                      {active
                        ? <MoveActiveIcon className="mr-2 h-5 w-5" ariaHidden=true />
                        : <MoveInactiveIcon className="mr-2 h-5 w-5" ariaHidden=true />}
                      {React.string("Move")}
                    </button>}
                </Menu.Item>
              </div>
              <div className="px-1 py-1">
                <Menu.Item>
                  {({active}) =>
                    <button
                      className={`${active
                          ? "bg-violet-500 text-white"
                          : "text-gray-900"} group flex w-full items-center rounded-md px-2 py-2 text-sm`}>
                      {active
                        ? <DeleteActiveIcon
                            className="mr-2 h-5 w-5 text-violet-400" ariaHidden=true
                          />
                        : <DeleteInactiveIcon
                            className="mr-2 h-5 w-5 text-violet-400" ariaHidden=true
                          />}
                      {React.string("Delete")}
                    </button>}
                </Menu.Item>
              </div>
            </Menu.Items>
          </Transition>
        </Menu>
      </div>
      <p className="mb-8">
        {React.string("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
eiusmod tempor incididunt ut labore et dolore magna aliqua. Dictumst
vestibulum rhoncus est pellentesque elit. Amet risus nullam eget felis eget
nunc lobortis. Nulla aliquet enim tortor at auctor urna nunc id. Elit ut
aliquam purus sit amet luctus. Vitae congue mauris rhoncus aenean vel elit
scelerisque mauris. Pharetra convallis posuere morbi leo. Neque egestas congue
quisque egestas. Quis ipsum suspendisse ultrices gravida dictum fusce.")}
      </p>
    </Card>
}

module ListboxExample = {
  type person = {name: string}

  let people = [
    {name: "Wade Cooper"},
    {name: "Arlene Mccoy"},
    {name: "Devon Webb"},
    {name: "Tom Cook"},
    {name: "Tanya Fox"},
    {name: "Hellen Schmidt"},
  ]

  @react.component
  let make = () => {
    let (selected, setSelected) = React.useState(_ => {name: "Wade Cooper"})

    <Card gradientFrom="amber-300" gradientTo="orange-500" className="pt-16">
      <div className="absolute top-8 left-8 right-8">
        <Listbox value={selected} onChange={value => setSelected(_ => value)}>
          <div className="relative mt-1">
            <Listbox.Button
              className="relative w-full cursor-default rounded-lg bg-white py-2 pl-3 pr-10 text-left shadow-md focus:outline-none focus-visible:border-indigo-500 focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-orange-300 sm:text-sm">
              <span className="block truncate"> {React.string(selected.name)} </span>
              <span
                className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
                <Heroicons.Solid.ChevronUpDownIcon
                  className="h-5 w-5 text-gray-400" ariaHidden=true
                />
              </span>
            </Listbox.Button>
            <Transition
              as_={React.Fragment.make}
              leave="transition ease-in duration-100"
              leaveFrom="opacity-100"
              leaveTo="opacity-0">
              <Listbox.Options
                className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                {people
                ->Belt.Array.mapWithIndex((personIdx, person) =>
                  <Listbox.Option
                    key={personIdx->Belt.Int.toString}
                    className={({active}) =>
                      `relative cursor-default select-none py-2 pl-10 pr-4 ${active
                          ? "bg-amber-100 text-amber-900"
                          : "text-gray-900"}`}
                    value={person}>
                    {({selected}) => <>
                      <span
                        className={`block truncate ${selected ? "font-medium" : "font-normal"}`}>
                        {React.string(person.name)}
                      </span>
                      {selected
                        ? <span
                            className="absolute inset-y-0 left-0 flex items-center pl-3 text-amber-600">
                            <Heroicons.Solid.CheckIcon className="h-5 w-5" ariaHidden=true />
                          </span>
                        : React.null}
                    </>}
                  </Listbox.Option>
                )
                ->React.array}
              </Listbox.Options>
            </Transition>
          </div>
        </Listbox>
      </div>
      <p className="mb-8">
        {React.string("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
eiusmod tempor incididunt ut labore et dolore magna aliqua. Dictumst
vestibulum rhoncus est pellentesque elit. Amet risus nullam eget felis eget
nunc lobortis. Nulla aliquet enim tortor at auctor urna nunc id. Elit ut
aliquam purus sit amet luctus. Vitae congue mauris rhoncus aenean vel elit
scelerisque mauris. Pharetra convallis posuere morbi leo. Neque egestas congue
quisque egestas. Quis ipsum suspendisse ultrices gravida dictum fusce.")}
      </p>
    </Card>
  }
}

external cast: 'a => 'b = "%identity"

module ComboxExample = {
  type person = {id: int, name: string}
  let people = [
    {id: 1, name: "Wade Cooper"},
    {id: 2, name: "Arlene Mccoy"},
    {id: 3, name: "Devon Webb"},
    {id: 4, name: "Tom Cook"},
    {id: 5, name: "Tanya Fox"},
    {id: 6, name: "Hellen Schmidt"},
  ]

  @react.component
  let make = () => {
    let (selected, setSelected) = React.useState(_ => people->Array.getUnsafe(0))
    let (query, setQuery) = React.useState(_ => "")

    let filteredPeople = if query == "" {
      people
    } else {
      people->Array.keep(person =>
        person.name
        ->String.toLowerCase
        ->String.replaceByRe(%re("/\s+/g"), "")
        ->String.includes(query->String.toLowerCase->String.replaceByRe(%re("/\s+/g"), ""))
      )
    }

    <Card gradientFrom="teal-400" gradientTo="cyan-400" className="pt-16">
      <div className="absolute top-16 w-72">
        <Combobox value={selected} onChange={v => setSelected(_ => v)}>
          <div className="relative mt-1">
            <div
              className="relative w-full cursor-default overflow-hidden rounded-lg bg-white text-left shadow-md focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-teal-300 sm:text-sm">
              <Combobox.Input
                className="w-full border-none py-2 pl-3 pr-10 text-sm leading-5 text-gray-900 focus:ring-0"
                displayValue={person => person.name}
                onChange={event =>
                  setQuery(_ => {
                    let target = event->Webapi.Dom.InputEvent.target
                    target->cast->Webapi.Dom.HtmlInputElement.value
                  })}
              />
              <Combobox.Button className="absolute inset-y-0 right-0 flex items-center pr-2">
                <Heroicons.Solid.ChevronUpDownIcon
                  className="h-5 w-5 text-gray-400" ariaHidden=true
                />
              </Combobox.Button>
            </div>
            <Transition
              as_={React.Fragment.make}
              leave="transition ease-in duration-100"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
              afterLeave={() => setQuery(_ => "")}>
              <Combobox.Options
                className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                {filteredPeople->Array.isEmpty && query != ""
                  ? <div className="relative cursor-default select-none py-2 px-4 text-gray-700">
                      {React.string("Nothing found.")}
                    </div>
                  : filteredPeople
                    ->Array.map(person =>
                      <Combobox.Option
                        key={person.id->Int.toString}
                        className={({active}) =>
                          `relative cursor-default select-none py-2 pl-10 pr-4 ${active
                              ? "bg-teal-600 text-white"
                              : "text-gray-900"}`}
                        value={person}>
                        {({selected, active}) => <>
                          <span
                            className={`block truncate ${selected
                                ? "font-medium"
                                : "font-normal"}`}>
                            {React.string(person.name)}
                          </span>
                          {selected
                            ? <span
                                className={`absolute inset-y-0 left-0 flex items-center pl-3 ${active
                                    ? "text-white"
                                    : "text-teal-600"}`}>
                                <Heroicons.Solid.CheckIcon className="h-5 w-5" ariaHidden=true />
                              </span>
                            : React.null}
                        </>}
                      </Combobox.Option>
                    )
                    ->React.array}
              </Combobox.Options>
            </Transition>
          </div>
        </Combobox>
      </div>
    </Card>
  }
}

module TabExample = {
  type item = {
    id: int,
    title: string,
    date: string,
    commentCount: int,
    shareCount: int,
  }
  type state = array<(string, array<item>)>

  @react.component
  let make = () => {
    let (categories, _) = React.useState(_ => [
      (
        "Recent",
        [
          {
            id: 1,
            title: "Does drinking coffee make you smarter?",
            date: "5h ago",
            commentCount: 5,
            shareCount: 2,
          },
          {
            id: 2,
            title: "So you've bought coffee... now what?",
            date: "2h ago",
            commentCount: 3,
            shareCount: 2,
          },
        ],
      ),
      (
        "Popular",
        [
          {
            id: 1,
            title: "Is tech making coffee better or worse?",
            date: "Jan 7",
            commentCount: 29,
            shareCount: 16,
          },
          {
            id: 2,
            title: "The most innovative things happening in coffee",
            date: "Mar 19",
            commentCount: 24,
            shareCount: 12,
          },
        ],
      ),
      (
        "Trending",
        [
          {
            id: 1,
            title: "Ask Me Anything: 10 answers to your questions about coffee",
            date: "2d ago",
            commentCount: 9,
            shareCount: 5,
          },
          {
            id: 2,
            title: "The worst advice we've ever heard about coffee",
            date: "4d ago",
            commentCount: 1,
            shareCount: 2,
          },
        ],
      ),
    ])

    <div className="w-screen px-2 py-16 sm:px-0">
      <Tab.Group>
        {_ => <>
          <Tab.List className="flex space-x-1 rounded-xl bg-blue-900/20 p-1">
            {_ =>
              categories
              ->Array.map(((category, _)) =>
                <Tab
                  key={category}
                  className={({selected}) =>
                    [
                      "w-full rounded-lg py-2.5 text-sm font-medium leading-5 text-blue-700",
                      "ring-white ring-opacity-60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2",
                      selected
                        ? "bg-white shadow"
                        : "text-blue-100 hover:bg-white/[0.12] hover:text-white",
                    ]->Array.join(" ")}>
                  {_ => React.string(category)}
                </Tab>
              )
              ->React.array}
          </Tab.List>
          <Tab.Panels className="mt-2">
            {_ =>
              categories
              ->Array.mapWithIndex((idx, (_, posts)) =>
                <Tab.Panel
                  key={idx->Int.toString}
                  className="rounded-xl bg-white p-3
                         ring-white ring-opacity-60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2">
                  {_ =>
                    <ul>
                      {posts
                      ->Array.map(post =>
                        <li
                          key={post.id->Int.toString}
                          className="relative rounded-md p-3 hover:bg-gray-100">
                          <h3 className="text-sm font-medium leading-5">
                            {React.string(post.title)}
                          </h3>
                          <ul
                            className="mt-1 flex space-x-1 text-xs font-normal leading-4 text-gray-500">
                            <li> {React.string(post.date)} </li>
                            <li> {React.string("U000B7")} </li>
                            <li>
                              {React.int(post.commentCount)}
                              {React.string(" comments")}
                            </li>
                            <li> {React.string("\U000B7")} </li>
                            <li>
                              {React.int(post.shareCount)}
                              {React.string(" shares")}
                            </li>
                          </ul>
                          <a
                            href="#"
                            className="absolute inset-0 rounded-md
                                 ring-blue-400 focus:z-10 focus:outline-none focus:ring-2"
                          />
                        </li>
                      )
                      ->React.array}
                    </ul>}
                </Tab.Panel>
              )
              ->React.array}
          </Tab.Panels>
        </>}
      </Tab.Group>
    </div>
  }
}

module SwitcherExample = {
  @react.component
  let make = () => {
    let (enabled, setEnabled) = React.useState(_ => false)

    <div className="py-16">
      <Switch
        checked={enabled}
        onChange={v => setEnabled(_ => v)}
        className={`${enabled ? "bg-teal-900" : "bg-teal-700"}
          relative inline-flex h-[38px] w-[74px] shrink-0 cursor-pointer rounded-full border-2 border-transparent transition-colors duration-200 ease-in-out focus:outline-none focus-visible:ring-2  focus-visible:ring-white focus-visible:ring-opacity-75`}>
        <span className="sr-only"> {React.string("Use setting")} </span>
        <span
          ariaHidden=true
          className={`${enabled ? "translate-x-9" : "translate-x-0"}
            pointer-events-none inline-block h-[34px] w-[34px] transform rounded-full bg-white shadow-lg ring-0 transition duration-200 ease-in-out`}
        />
      </Switch>
    </div>
  }
}

module App = {
  @react.component
  let make = () =>
    <div>
      <div className="flex gap-8">
        <MenuExample />
        <ListboxExample />
      </div>
      <div className="flex">
        <TabExample />
      </div>
      <div className="flex">
        <SwitcherExample />
      </div>
    </div>
}

ReactDOM.querySelector("#app")
->Belt.Option.map(rootElement => {
  let root = ReactDOM.Client.createRoot(rootElement)
  ReactDOM.Client.Root.render(root, <App />)
})
->ignore
