;;; Directory Local Variables            -*- no-byte-compile: t -*-
;;; For more information see (info "(emacs) Directory Variables")

((nil . ((projectile-project-compilation-cmd . "source .envrc; make test")))
  (json-mode . ((js-indent-level . 2)))
  (mhtml-mode . ((css-indent-offset . 2)
                  (js-indent-level . 2))))
